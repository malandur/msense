# README #

msense app

### How do I get set up? ###
* Run the db.sql file inside resources to setup database

### To Create a Runable jar run
mvn clean package

### To Run the program 
java -jar target/msense.0.0.1.jar

### Provide Arguments as vm arguments while running 

* spring.datasource.url = jdbc:mysql://localhost:3306/mydb?useSSL=false
* spring.datasource.username = root
* spring.datasource.password = root
* spring.datasource.driver-class-name=com.mysql.jdbc.Driver

Sample: java -jar -Dspring.datasource.username=test target/msense.0.0.1.jar

