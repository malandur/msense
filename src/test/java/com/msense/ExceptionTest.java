package com.msense;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.msense.controller.ClientRegisterHandler;
import com.msense.dao.UserRepository;
import com.msense.dto.LatLong;
import com.msense.dto.ValidationError;
import com.msense.service.UserExpression;
import com.msense.service.UserService;
import com.msense.service.ValidationErrorBuilder;

public class ExceptionTest {

	ObjectMapper mapper = new ObjectMapper();

	@Mock
	UserService userService;

	@Mock
	UserRepository userRepo;

	@Mock
	PasswordEncoder passwordEncoder;

	ClientRegisterHandler clientRegister;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		clientRegister = new ClientRegisterHandler();
		clientRegister.setForTest(userRepo, passwordEncoder);
	}

	@Test
	public void testValidationObject() throws JsonProcessingException {
		ValidationError error = ValidationErrorBuilder.fromSingleError("T1", "T1");
		ValidationError error2 = new ValidationError("T1");
		error2.addValidationError("T1");
		assertEquals(mapper.writeValueAsString(error), mapper.writeValueAsString(error2));
	}

	@Test
	public void testUserService() {
		verify(userService, times(0)).storeData(new LatLong(12d, 1d, UserExpression.SAD), "3342");
	}

	@Test
	public void testMockClientRegister() throws Exception {
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(clientRegister).build();
		mockMvc.perform(get("/register?deviceId=3223")).andExpect(status().isOk());
	}

	@Test
	public void testMockClientRegisterFailed() throws Exception {
		MockMvc mockMvc = MockMvcBuilders.standaloneSetup(clientRegister).build();
		mockMvc.perform(get("/register")).andExpect(status().is4xxClientError());
	}

}
