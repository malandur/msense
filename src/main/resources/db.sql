
-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`user` (
  `deviceId` VARCHAR(32) NOT NULL,
  `password` VARCHAR(60) NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`deviceId`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`userdata`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`userdata` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `latitude` DOUBLE NULL,
  `longitude` DOUBLE NULL,
  `location` VARCHAR(60) NULL,
  `user_deviceId` VARCHAR(32) NOT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `userExpression` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_userdata_user_idx` (`user_deviceId` ASC),
  INDEX `locationQuery` (`user_deviceId` ASC, `location` ASC),
  CONSTRAINT `fk_userdata_user`
    FOREIGN KEY (`user_deviceId`)
    REFERENCES `mydb`.`user` (`deviceId`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;