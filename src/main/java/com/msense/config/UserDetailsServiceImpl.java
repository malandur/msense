package com.msense.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.msense.dao.UserRepository;
import com.msense.model.User;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	UserRepository userRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
			User user = userRepo.findOne(username);
			if (user != null)
				return new org.springframework.security.core.userdetails.User(user.getDeviceid(), user.getPassword(),
						Arrays.asList(new SimpleGrantedAuthority("USER")));
		} catch (Exception e) {
		}
		throw new UsernameNotFoundException("Unable to find user " + username);
	}

}
