package com.msense.config;

import java.util.HashMap;
import java.util.Map;

import javax.validation.UnexpectedTypeException;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.msense.dto.ValidationError;
import com.msense.exception.UserRegistered;
import com.msense.service.ValidationErrorBuilder;

@ControllerAdvice
@Component
public class GlobalExceptionHandler {

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public Map<String, Object> handle(MissingServletRequestParameterException exception) {
		Map<String, Object> m = new HashMap<String, Object>();
		m.put("error", exception.getLocalizedMessage());
		return m;
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ValidationError handle(UserRegistered exception) {
		return ValidationErrorBuilder.fromSingleError("User Already Registered", exception.getLocalizedMessage());
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ValidationError handle(UnexpectedTypeException exception) {
		return ValidationErrorBuilder.fromSingleError("Invalid parameter type used", exception.getLocalizedMessage());
	}

	@ExceptionHandler
	@ResponseBody
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	public ValidationError handle(HttpMessageNotReadableException exception) {
		return ValidationErrorBuilder.fromSingleError("Invalid parameter type used",
				exception.getMostSpecificCause().getMessage());
	}

}