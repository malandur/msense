package com.msense.dao;

import java.util.Map;

import com.msense.dto.ProximityData;

public interface UserDataRepositoryCustom {
	public Map<String, Long> getFreqency(String deviceId);

	public ProximityData getProximity(String deviceId);
}
