package com.msense.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.msense.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, String> {

}
