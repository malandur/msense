package com.msense.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.msense.dto.ProximityData;
import com.msense.service.UserExpression;

@Repository
@Transactional(readOnly = true)
@SuppressWarnings("unchecked")
public class UserDataRepositoryImpl implements UserDataRepositoryCustom {

	@PersistenceContext
	EntityManager emanager;

	@Value("${msense.proximity.limit:5}")
	int limit;

	@Override
	public Map<String, Long> getFreqency(String deviceId) {
		javax.persistence.Query q = emanager.createQuery(
				"select userdata.userExpression,count(userdata.userExpression) from Userdata as userdata where userdata.user.deviceid=:deviceId	group by userdata.userExpression");
		q.setParameter("deviceId", deviceId);
		List<Object[]> l = q.getResultList();
		Map<String, Long> map = new HashMap<>();
		for (Object[] o : l) {
			UserExpression exp = (UserExpression) o[0];
			Long count = (Long) o[1];
			map.put(exp.toString(), count);
		}
		return map;
	}

	@Override
	public ProximityData getProximity(String deviceId) {
		Long total = 0L;
		javax.persistence.Query q = emanager.createQuery(
				"select userdata.location,count(userdata.location) as c  from Userdata as userdata where userdata.user.deviceid=:deviceId and userdata.userExpression='HAPPY' group by userdata.location order by c ASC");
		q.setMaxResults(limit);
		q.setParameter("deviceId", deviceId);

		List<Object[]> l = q.getResultList();
		Map<String, Long> map = new HashMap<>();
		if (l != null) {
			for (Object[] o : l) {
				String exp = (String) o[0];
				Long count = (Long) o[1];
				map.put(exp.toString(), count);
			}
		}
		q = emanager.createQuery(
				"select count(userdata.location) from Userdata as userdata where userdata.user.deviceid=:deviceId and userdata.userExpression='HAPPY'");
		q.setParameter("deviceId", deviceId);
		List<Long> l2 = q.getResultList();
		if (l2 != null && l2.size() > 0) {
			total = l2.get(0);
		}
		return new ProximityData(total, map);
	}

}
