package com.msense.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.msense.model.Userdata;

@Repository
public interface UserDataRepository extends JpaRepository<Userdata, String>, UserDataRepositoryCustom {

}
