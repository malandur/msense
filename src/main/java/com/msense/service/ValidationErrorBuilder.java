package com.msense.service;

import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;

import com.msense.dto.ValidationError;

public class ValidationErrorBuilder {

	public static ValidationError fromBindingErrors(Errors errors) {
		ValidationError error = new ValidationError("Validation failed. " + errors.getErrorCount() + " error(s)");
		for (ObjectError objectError : errors.getAllErrors()) {
			error.addValidationError(objectError.getDefaultMessage());
		}
		return error;
	}

	public static ValidationError fromSingleError(String errMsg, String error) {
		ValidationError err = new ValidationError(errMsg);
		err.addValidationError(error);
		return err;
	}

}