package com.msense.service;

import com.msense.exception.LocationUnavailable;

/**
 * 
 * @author mithu
 * 
 *         Service that enables us to provide latitude and longitude to get
 *         nearest location
 */
public interface LocationService {

	public String getProximity(double latitude, double longitude) throws LocationUnavailable;

}
