package com.msense.service;

import java.util.Random;

import org.springframework.stereotype.Service;

/**
 * 
 * @author mithu
 * 
 *         A dummy method used to generate some random locationType
 */
@Service
public class LocationServiceImpl implements LocationService {

	String locations[] = new String[] { "home", "office", "shopping center", "park" };

	@Override
	public String getProximity(double latitude, double longitude) {
		Random random = new Random();
		return locations[random.nextInt(locations.length)];
	}

}
