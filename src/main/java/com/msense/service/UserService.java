package com.msense.service;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.msense.dao.UserDataRepository;
import com.msense.dto.LatLong;
import com.msense.exception.LocationUnavailable;
import com.msense.model.User;
import com.msense.model.Userdata;

@Service
// Make task as async
public class UserService {

	@Autowired
	UserDataRepository userDataRepo;

	@Autowired
	LocationService locationService;

	public void storeData(LatLong latlong, String deviceId) {
		// TODO : Encapsulate with spring-retry
		String location = "UNKNOWN";
		try {
			location = locationService.getProximity(latlong.getLatitude(), latlong.getLongitude());
		} catch (LocationUnavailable e) {
			e.printStackTrace();
		}
		Userdata data = new Userdata(latlong.getLatitude(), latlong.getLongitude(), location, latlong.getExpression(),
				new User(deviceId), new Date());
		userDataRepo.save(data);
	}
}
