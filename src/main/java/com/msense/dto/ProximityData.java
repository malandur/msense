package com.msense.dto;

import java.util.Map;

public class ProximityData {
	Long total;
	Map<String, Long> data;

	public ProximityData() {
		super();
	}

	public ProximityData(Long total, Map<String, Long> data) {
		super();
		this.total = total;
		this.data = data;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public Map<String, Long> getData() {
		return data;
	}

	public void setData(Map<String, Long> data) {
		this.data = data;
	}

}
