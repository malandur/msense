package com.msense.dto;

import lombok.Data;

@Data
public class Status {
	String status;

	public Status(String status) {
		super();
		this.status = status;
	}

}
