package com.msense.dto;

import javax.validation.constraints.NotNull;

import com.msense.service.UserExpression;

public class LatLong {

	@NotNull(message = "latitude  cannot be empty")
	Double latitude;

	@NotNull(message = "longitude cannot be empty")
	Double longitude;

	UserExpression expression = UserExpression.NEUTRAL;

	public LatLong() {
		super();
	}

	public LatLong(Double latitude, Double longitude, UserExpression expression) {
		super();
		this.latitude = latitude;
		this.longitude = longitude;
		this.expression = expression;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public UserExpression getExpression() {
		return expression;
	}

	public void setExpression(UserExpression expression) {
		this.expression = expression;
	}

}
