package com.msense.exception;

public class UserRegistered extends Exception {

	private static final long serialVersionUID = 1400339630240661814L;

	public UserRegistered(String deviceId) {
		super("DeviceId already registered " + deviceId);
	}

}
