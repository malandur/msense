package com.msense.model;
// Generated Feb 9, 2018 11:29:36 AM by Hibernate Tools 5.2.8.Final

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * User generated by hbm2java
 */
@Entity
@Table(name = "user")
public class User implements java.io.Serializable {

	private String deviceid;
	private String password;
	private Date createTime;

	public User() {
	}

	public User(String deviceid) {
		this.deviceid = deviceid;
	}

	public User(String deviceid, String password, Date createTime) {
		this.deviceid = deviceid;
		this.password = password;
		this.createTime = createTime;
	}

	@Id
	@Column(name = "deviceid", unique = true, nullable = false, length = 32)
	public String getDeviceid() {
		return this.deviceid;
	}

	public void setDeviceid(String deviceid) {
		this.deviceid = deviceid;
	}

	@Column(name = "password", length = 60)
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", length = 19)
	public Date getCreateTime() {
		return this.createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

}
