package com.msense.controller;

import java.util.Date;

import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.msense.dao.UserRepository;
import com.msense.dto.AuthToken;
import com.msense.exception.UserRegistered;
import com.msense.model.User;

@RestController
@Validated
public class ClientRegisterHandler {

	@Value("${msense.client.pass.len:8}")
	int minPassLen;
	@Value("${msense.client.pass.len:10}")
	int maxPassLen;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	UserRepository userRepo;

	@RequestMapping("/register")
	public AuthToken getAuthToken(@RequestParam(value = "deviceId", required = true) String deviceId)
			throws UserRegistered {
		String password = RandomStringUtils.randomAlphabetic(minPassLen, maxPassLen);
		try {
			if (userRepo.findOne(deviceId) == null) {
				userRepo.save(new User(deviceId, passwordEncoder.encode(password), new Date()));
				return new AuthToken(deviceId, password);
			}
		} catch (Exception e) {
		}
		throw new UserRegistered(deviceId);
	}

	// TODO : Change to use the springrunnner along with the mockbean annotation
	public void setForTest(UserRepository userRepo, PasswordEncoder passwordEncoder) {
		this.userRepo = userRepo;
		this.passwordEncoder = passwordEncoder;
	}

}
