package com.msense.controller;

import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.msense.dao.UserDataRepository;
import com.msense.dto.LatLong;
import com.msense.dto.ProximityData;
import com.msense.service.UserService;
import com.msense.service.ValidationErrorBuilder;

@RestController
public class ClientRequestHandler {

	@Autowired
	UserService userService;

	@Autowired
	UserDataRepository userDataRepo;

	@RequestMapping(value = "/user/data", method = RequestMethod.POST)
	public ResponseEntity<?> postUserData(@Valid @RequestBody LatLong latLong, Errors errors, Authentication auth) {
		if (errors.hasErrors()) {
			return ResponseEntity.badRequest().body(ValidationErrorBuilder.fromBindingErrors(errors));
		}
		userService.storeData(latLong, auth.getName());
		return ResponseEntity.ok("");
	}

	@RequestMapping(value = "/user/frequency", method = RequestMethod.GET)
	public Map<String, Long> userFrequency(Authentication auth) {
		return userDataRepo.getFreqency(auth.getName());
	}

	@RequestMapping(value = "/user/proximity", method = RequestMethod.GET)
	public ProximityData userProximity(Authentication auth) {
		return userDataRepo.getProximity(auth.getName());
	}

}
